using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace Task1
{
    /// <summary>
    /// static class that represents determinant functionality
    /// </summary>
    public static class Determinant
    {
        #region Methods

            /// <summary>
            /// Method for getting determinant value
            /// </summary>
            /// <param name="matrix"> Matrix which determinant is calculating for</param>
            /// <returns> Determinant value</returns>
            public static double GetDeterminantValue(double[,] matrix)
            {
                //Calculating matrix order
                int order = int.Parse(Math.Sqrt(matrix.Length).ToString());

                if (order == 2)
                {
                    return matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
                }
                else if (order > 2)
                {
                    double determinantValue = 0;

                    for (int i = 0; i < order; i++)
                    {
                        //Creating smaller matrix without 0 row and i column
                        double[,] smallerMatrix = CreateSmallerMatrix(matrix, 0, i);

                        //Calculating determinant
                        determinantValue += ElementSign(0, i) * matrix[0, i] * GetDeterminantValue(smallerMatrix);
                    }

                    return determinantValue;
                }
                else
                {
                    return matrix[0, 0];
                }
            }

            /// <summary>
            /// Method for creating smaller matrix
            /// </summary>
            /// <param name="matrix"> Matrix which is creating smaller from</param>
            /// <param name="row">Number of row which shouldn't be included into new matrix</param>
            /// <param name="column">Number of column which shouldn't be included into new matrix</param>
            /// <returns> New matrix</returns>
            public static double[,] CreateSmallerMatrix(double[,] matrix, int row, int column)
            {
                 int order = int.Parse(Math.Sqrt(matrix.Length).ToString());
                 double[,] smallerMatrix = new double[order - 1, order - 1];
                 int i = 0, j = 0;

                 for (int rowNumber = 0; rowNumber < order; rowNumber++, i++)
                 {
                    if (rowNumber != row)
                    {
                       j = 0;

                       for (int columnNumber = 0; columnNumber < order; columnNumber++)
                       {
                          if (columnNumber != column)
                          {
                             smallerMatrix[i, j] = matrix[rowNumber, columnNumber];
                             j++;
                          }
                       }
                    }
                    else
                    {
                       i--;
                    }
                 }

                 return smallerMatrix;

            }

        #endregion

        #region Helpers

            /// <summary>
            /// Method for defining the sign of element
            /// </summary>
            /// <param name="row">Number of row where element is located</param>
            /// <param name="column">Number of column where element is located</param>
            /// <returns></returns>
            private static int ElementSign(int row, int column)
            {
                if ((row + column) % 2 == 0 )
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }

        #endregion
    }

    /// <summary>
    /// Class that represents matrix
    /// </summary>
    public class Matrix : IEnumerable<double>
    {
        #region Fields

            /// <summary>
            /// Two dimensional array for representation matrix
            /// </summary>
            private double[,] _matrix;

        #endregion

        #region Properties

            /// <summary>
            /// Property for getting / (setting) number of matrix rows
            /// </summary>
            public int Rows { get; private set; }

            /// <summary>
            /// Property for getting / (setting) number of matrix columns
            /// </summary>
            public int Columns { get; private set; }

        #endregion

        #region Constructors
            
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="rows"> Number of rows</param>
            /// <param name="columns"> Number of columns </param>
            public Matrix(int rows, int columns)
            {
                Rows = rows;
                Columns = columns;
                _matrix = new double[Rows, Columns];
            }
            
            /// <summary>
            /// Overloaded constructur for creating square matrix
            /// </summary>
            /// <param name="order"> Order of matrix</param>
            public Matrix(int order) : this(order, order)
            {

            }
            
            /// <summary>
            /// Overloaded constructor 
            /// </summary>
            /// <param name="matrix"> Two-dimensional array for representation matrix</param>
            public Matrix(double[,] matrix)
            {
                _matrix = matrix;
                Rows = _matrix.GetLength(0);
                Columns = _matrix.GetLength(1);
            }

        #endregion

        #region Methods

            /// <summary>
            /// Method for getting k order minor value
            /// </summary>
            /// <param name="k"> Number of order</param>
            /// <returns> Minor value</returns>
            public double GetKOrderMinor(int k)
            {
                int min;

                if(Rows > Columns)
                {
                    min = Columns;
                }
                else
                {
                    min = Rows;
                }

                if (k > min)
                {
                    throw new ArgumentOutOfRangeException("Order is to mutch.");
                }
                else
                {
                    double[,] minor = new double[k, k];
                    List<int> rows = GetRandomValues(k,Rows);
                    List<int> columns = GetRandomValues(k,Columns);
                    int i = 0, j = 0;

                    foreach (int row in rows)
                    {
                        j = 0;

                        foreach (int column in columns)
                        {
                            minor[i, j] = _matrix[row, column];
                            j++;
                        }

                        i++;
                    }

                    return Determinant.GetDeterminantValue(minor);
                }
            }

            /// <summary>
            /// Method for getting complementary minor
            /// </summary>
            /// <param name="k"> Number of order</param>
            /// <returns> Minor value </returns>
            public double GetComplementlyMinor(int k)
            {
                //If matrix is not square
                if (Rows != Columns)
                {
                    throw new InvalidOperationException("Matrix should be square for calculating complemntaly minor");
                }
                else
                {
                    double[,] minor = new double[Rows - k, Columns - k];
                    List<int> rows = GetRandomValues(k,Rows);
                    List<int> columns = GetRandomValues(k,Columns);
                    int i = 0, j = 0;

                    foreach (int row in rows)
                    {
                        j = 0;
                        if (!rows.Contains(row))
                        {
                            foreach (int column in columns)
                            {
                                if (!columns.Contains(column))
                                {
                                    minor[i, j] = _matrix[row, column];
                                    j++;
                                }

                            }
                            i++;
                        }

                    }

                    return Determinant.GetDeterminantValue(minor);
                }
            }

            /// <summary>
            /// Method for calculating element minor
            /// </summary>
            /// <param name="row"> Number of element's row</param>
            /// <param name="column"> Number of element's row </param>
            /// <returns></returns>
            public double GetElementMinor(int row, int column)
            {
                if (Rows == Columns)
                {
                    return Determinant.GetDeterminantValue(Determinant.CreateSmallerMatrix(_matrix, row, column));
                }
                else
                {
                    throw new InvalidOperationException("Matrix should be square for calculating complemntaly minor");
                }    
            }
            
            /// <summary>
            /// Overriding ToString method
            /// </summary>
            /// <returns> String representation of matrix</returns>
            public override string ToString()
            {
                StringBuilder matrixString = new StringBuilder();

                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Columns; j++)
                    {
                        matrixString.AppendFormat("{0,5}",_matrix[i, j]);
                        //matrixString.Append("  ");
                    }

                    matrixString.Append(Environment.NewLine);
                }

                return matrixString.ToString();
            }

            #region Implementing IEnumerable<double> interface

                public IEnumerator<double> GetEnumerator()
                {
                    foreach (var element in _matrix)
                    {
                        yield return element;
                    }
                }

                IEnumerator IEnumerable.GetEnumerator()
                {
                    return this.GetEnumerator();
                }

        #endregion

            /// <summary>
            /// Indexer method
            /// </summary>
            /// <param name="row"> Row number of element for getting/setting</param>
            /// <param name="column">Column number of element for getting/setting</param>
            /// <returns></returns>
            public double this[int row, int column]
            {
                get { return _matrix[row, column]; }
                set { _matrix[row, column] = value; }
            }

        #endregion

        #region Helpers

            /// <summary>
            /// Method for getting random values
            /// </summary>
            /// <param name="max"> Number of generated values</param>
            /// <returns> List of generated values</returns>
            private List<int> GetRandomValues(int max,int maxValue)
            {
                List<int> values = new List<int>();
                Random rand = new Random();

                while (values.Count < max)
                {
                    int randomValue;

                    do
                    {

                        randomValue = rand.Next(0, maxValue);

                    } while (values.Contains(randomValue));

                    values.Add(randomValue);
                }

                return values;
            }

        #endregion
    }
    class Program
    {
        static void Main(string[] args)
        {
            //Creating matrix object
            Matrix matrix = new Matrix(4);
            Random random = new Random();

            //Filling matrix with random values
            for(int i=0; i<matrix.Rows; i++)
            {
                for(int j=0; j<matrix.Columns; j++)
                {
                    matrix[i, j] = random.Next(0,100);
                }
            }

            //Output matrix on the screen
            Console.WriteLine("Matrix:\n" + matrix.ToString());

            Console.WriteLine("\n********************************************\n");

            try
            {
                //Calculating  k order minor
                Console.WriteLine("Minor of 2 order is {0}.", matrix.GetKOrderMinor(2).ToString());
            }
            catch(ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                return;
            }

            try
            {
                //Calculating complemently minor and element's minor
                Console.WriteLine("Complemently minor to 2 order minor is {0}.",matrix.GetComplementlyMinor(2).ToString());
                Console.WriteLine("Minor of element (1,3) is {0}.", matrix.GetElementMinor(1,3).ToString());
            }
            catch(InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                return;
            }

            Console.ReadLine();
            
        }
    }
}