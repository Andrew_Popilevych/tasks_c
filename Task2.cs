using System;

namespace Task2
{
    /// <summary>
    /// Class for representation polyom
    /// </summary>
    public class Polynom
    {
        #region Fields
            
            /// <summary>
            /// Coeficients array
            /// </summary>
            private double[] _coeficients;

            /// <summary>
            /// Polynom power
            /// </summary>
            private int _power;

        #endregion

        #region Properties
            
            /// <summary>
            /// Propery for getting polynom's coeficients
            /// </summary>
            public double[] Coeficiens
            {
                get { return _coeficients; }
            }

            /// <summary>
            /// Property for getting polynom's power
            /// </summary>
            public int Power
            {
                get { return _power; }
            }

        #endregion

        #region Constructor
            
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="power"> Polynom's power</param>
            /// <param name="coeficients">Polynom's coeficients</param>
            public Polynom(int power, params double[] coeficients)
            {
                _power = power;

                //If polynom is complete
                if (coeficients.Length - 1 == power)
                {
                    _coeficients = coeficients;
                }
                else
                {   
                    _coeficients = new double[power + 1];
                    int i = 0;

                    for (i = 0; i < coeficients.Length; i++)
                    {
                        _coeficients[i] = coeficients[i];
                    }

                    //Filling empty places by zero
                    while (i <= power)
                    {
                        _coeficients[i++] = 0;
                    }
                }

            }

            /// <summary>
            /// Overloaded constructor
            /// </summary>
            /// <param name="coeficients"> Coeficients array</param>
            public Polynom(double[] coeficients) : this(coeficients.Length - 1, coeficients)
            {
            }

        #endregion

        #region Methods
            
            /// <summary>
            /// Method for getting polynom value for proper argument
            /// </summary>
            /// <param name="x"> Argument value</param>
            /// <returns> Polynom value</returns>
            public double GetPolynomValue(double x)
            {
                int power = _power;
                double sum = 0;

                foreach (var coeficient in _coeficients)
                {
                    sum += coeficient * Math.Pow(x, power--);
                }
                return sum;
            }
            
            /// <summary>
            /// Adding two polynoms
            /// </summary>
            /// <param name="polynom1"> First polynom</param>
            /// <param name="polynom2"> Second polynom</param>
            /// <returns> New polynom which is the sum of first and second</returns>
            public static  Polynom operator +(Polynom polynom1, Polynom polynom2)
            {
                //if first polynom power mutch than second
                if (polynom1 >= polynom2)
                {
                    int power = polynom1.Power, index = 0;
                    var resultCoeficients = new double[power+1];
                    var coeficientsOfPolynom1 = polynom1.Coeficiens;
                    var coeficientsOfPolynom2 = polynom2.Coeficiens;

                    while (power > polynom2.Power)
                    {
                        resultCoeficients[index] = polynom1.Coeficiens[index++];
                        power--;
                    }

                    foreach (var element in coeficientsOfPolynom2)
                    {
                        resultCoeficients[index] = element + coeficientsOfPolynom1[index++];
                    }

                    return new Polynom(polynom1.Power, resultCoeficients);
                }
                else
                {
                    return polynom2 + polynom1;
                }
        }

            /// <summary>
            /// Subtraction two polynoms
            /// </summary>
            /// <param name="polynom1">First polynom</param>
            /// <param name="polynom2">Second polynom</param>
            /// <returns>New polynom which is the subtraction of first and second</returns>
            public static  Polynom operator -(Polynom polynom1, Polynom polynom2)
            {
                var inverseCoefitients = new double[polynom2.Power + 1];
                
                //Inversing second polynom coeficients
                for (int i = 0; i <= polynom2.Power; i++)
                {
                    inverseCoefitients[i] = -1 * polynom2.Coeficiens[i];
                }
                return polynom1 + new Polynom(inverseCoefitients);
            }

            /// <summary>
            /// Multiplication two polynoms
            /// </summary>
            /// <param name="polynom1">First polynom</param>
            /// <param name="polynom2">Second polynom</param>
            /// <returns>New polynom which is the multiplication of first and second</returns>
            public static Polynom operator *(Polynom polynom1, Polynom polynom2)
            {
                double[] result = new double[polynom1.Power + polynom2.Power + 1];

                for (int i = 0; i <= polynom1.Power; i++)
                {
                    for (int j = 0; j <= polynom2.Power; j++)
                    {
                        result[i + j] += polynom1.Coeficiens[i] * polynom2.Coeficiens[j];
                    }
                }

                return new Polynom(result);
            }
            
            /// <summary>
            /// Output polynom on console screen
            /// </summary>
            public void ShowPolynomOnConsole()
            {
                int power = _power, i;

                for (i = 0; i < Power; i++,power--)
                {
                    if (_coeficients[i] != 0)
                    {
                         
                        Console.Write("{0}* X^{1} + ", _coeficients[i], power);
                    }
                    
                }

                 Console.WriteLine("{0}.", _coeficients[i]);
          
            }
            
            /// <summary>
            /// Comparing two polynoms by power
            /// </summary>
            /// <param name="polynom1"> First polynom</param>
            /// <param name="polynom2"> Second polynom</param>
            /// <returns> True if first polynom power is bigger or equal</returns>
            public static  bool operator >=(Polynom polynom1, Polynom polynom2)
            {
                return polynom1.Power >= polynom2.Power;
            }

            /// <summary>
            /// Comparing two polynoms by power
            /// </summary>
            /// <param name="polynom1"> First polynom</param>
            /// <param name="polynom2"> Second polynom</param>
            /// <returns> True if first polynom power is less or equal</returns>
            public static  bool operator<=(Polynom polynom1, Polynom polynom2)
            {
                return polynom1.Power <= polynom2.Power;
            }

        #endregion
    }

    class Program
    {
        static void Main(string[] args)
        {
            double[] firstPolynomCoeficients = new double[] {3,1,7};
            double[] secondPolynomCoeficients = new double[] { 2,1 };

            Polynom polynom1 = new Polynom(firstPolynomCoeficients);
            Polynom polynom2 = new Polynom(4, secondPolynomCoeficients);

            Console.WriteLine("Polynomes:");
            polynom1.ShowPolynomOnConsole();
            Console.WriteLine("********************************************");
            polynom2.ShowPolynomOnConsole();

            Console.Write("Polynoms sum is: ");
            (polynom1 + polynom2).ShowPolynomOnConsole();
            Console.WriteLine("********************************************");

            Console.Write("Polynoms subtraction is: ");
            (polynom1 - polynom2).ShowPolynomOnConsole();
            Console.WriteLine("********************************************");

            Console.Write("Polynoms multiplication is: ");
            (polynom1 * polynom2).ShowPolynomOnConsole();
            Console.WriteLine("********************************************\n");

            Random rand = new Random();
            double x = rand.Next(-10, 10);

            Console.WriteLine("When argument = {0}, polynom1 value is {1}.",x, polynom1.GetPolynomValue(x));
            Console.WriteLine("When argument = {0}, polynom2 value is {1}.",x, polynom2.GetPolynomValue(x));
            Console.ReadLine();
        }
    }
}
