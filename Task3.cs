using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task3
{
    /// <summary>
    /// class vector, that holds elements from 0 to 2
    /// </summary>
    public class Vector:IEnumerable<int>
    {
        #region Fields

            /// <summary>
            /// Vector of elemements from {0,1,2} plural 
            /// </summary>
           private int[] _vector;

        #endregion

        #region Properties

            /// <summary>
            /// Read only property for getting array of elements
            /// </summary>
            public int[] VectorElements
            {
                get { return _vector; }
            }

            /// <summary>
            /// Read only property for getting total number of elements in vector
            /// </summary>
            public int Length
            {
                get { return _vector.Length; }
            }

            /// <summary>
            /// Property for getting vector capacity
            /// </summary>
            public int Capacity { get; private set;}
        
        #endregion

        #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="size"> Desired array size</param>
            public Vector(int size)
            {
                _vector = new int[size];
                Capacity = size;
            }

            /// <summary>
            /// Overloaded constructor
            /// </summary>
            /// <param name="array"> Elements of vector </param>
            public Vector(params int[] array)
            {
                var sequence = from element in array where element > 2 || element < 0 select element;
                if (sequence.Count() == 0)
                {
                    _vector = array;
                    Capacity = array.Length;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Element should be in range of [0,2].");
                }
            }

        #endregion

        #region Methods

            /// <summary>
            /// Indexer method
            /// </summary>
            /// <param name="index"> Index of element for get/set value</param>
            /// <returns></returns>
            public int this[int index]
            {
                get { return _vector[index]; }

                set
                {
                    int temp = value;

                    //If element is out of range - exception
                    if(temp < 0 || temp > 2)
                    {
                        throw new ArgumentOutOfRangeException("Element should be in range of [0,2].");
                    }
                    _vector[index] = temp;
                }
            }

            /// <summary>
            /// Method for getting number of elements in vector with proper value
            /// </summary>
            /// <param name="value"> number ofr comparing</param>
            /// <returns> number of elements </returns>
            public int GetNumberOfElementsWithProperValue(int value)
            {
                var elements = from element in _vector where element == value select element;
                return elements.Count();
            }

            /// <summary>
            /// Method for checking vectors whether they are ortogonal
            /// </summary>
            /// <param name="vector2"> vector for comparing</param>
            /// <returns> true if vectors are ortogonal</returns>
            public static bool AreOrtogonal (Vector vector1,Vector vector2)
            {
                if(vector1.VectorElements.Length != vector2.VectorElements.Length)
                {
                    throw new ArgumentException("Vectors have different size.");
                }
                else
                {
                    return ScalarMultiplication(vector1.VectorElements, vector2.VectorElements) == 0;
                }  
            }

            /// <summary>
            /// Method for searching interscction of two vectors
            /// </summary>
            /// <param name="vector"> vector for searching intersection</param>
            /// <returns> new array which is intersection of current vector and vector passed in parametr</returns>
            public Vector Intersection (Vector vector)
            {
                int[] vector1 = this.VectorElements;
                int[] vector2 = vector.VectorElements;

                if(vector1.Length !=vector2.Length)
                {
                    throw new ArgumentException("Vectors have different size.");
                }

                int[] intersection = new int[vector2.Length];

                for(int i=0; i<vector1.Length; i++)
                {
                    if((vector1[i]==1 && vector2[i]==1) ||
                       (vector1[i] == 1 && vector2[i] == 2) ||
                       (vector1[i] == 2 && vector2[i] == 1))
                    {
                        intersection[i] = 1;

                    }
                    else if (vector1[i] == 0 || vector2[i] == 0)
                    {
                        intersection[i] = 0;
                    }
                    else
                    {
                        intersection[i] = 2;
                    }
                }

                return new Vector(intersection);
            }

            /// <summary>
            /// Overriding ToString() method
            /// </summary>
            /// <returns>string representation of vector </returns>
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                foreach (var element in _vector)
                {
                    string elementAsString = string.Format("{0} ", element.ToString());
                    sb.Append(elementAsString);
                }

                return sb.ToString();
            }

            #region Implementing IEnumerable<int> interface

                public IEnumerator<int> GetEnumerator()
                {
                     foreach (int element in _vector)
                     {
                         yield return element;
                     }
                 }
             
                /// <summary>
                /// Explicitly implementation
                /// </summary>
                /// <returns>Object that implements IEnumerable</returns>
                 IEnumerator IEnumerable.GetEnumerator()
                 {
                     return this.GetEnumerator();
                 }
            #endregion
           #endregion

        #region Helpers
            
            /// <summary>
            /// Method for checking whether value is included in set {0,1,2}
            /// </summary>
            /// <param name="value"> Value that is being checked</param>
            /// <returns> true in value doesnt belong the set</returns>
            private bool IsOutOfRange(int value)
            {
                return (value < 0 || value > 2);
            }
            
            /// <summary>
            /// Method for searching scalar multiplication
            /// </summary>
            /// <param name="vector1"> first vector</param>
            /// <param name="vector2"> second vector</param>
            /// <returns> Value of scalar multiplicaton </returns>
            private static int ScalarMultiplication(int[] vector1, int[] vector2)
            {
                int multiplication = 0;

                for(int i=0; i<vector1.Length; i++)
                {
                    multiplication += (vector1[i] * vector2[i]);
                }

                return multiplication;
            }

        #endregion
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Creating first vector
            Vector vector1 = new Vector(6);

            try
            {
                //Creating second vector
                Vector vector2 = new Vector(1, 2, 1, 1, 0, 2);

                //Filling first vector with random values from {0,1,2} set
                Random rand = new Random();

                for (int i = 0; i < vector1.Capacity; i++)
                {
                    vector1[i] = rand.Next(0, 3);
                }

                //Output first vector
                Console.WriteLine("First vector:");
                foreach (var element in vector1)
                {
                    Console.WriteLine("{0} ", element.ToString());
                }

                Console.WriteLine("***********************************");

                //Output second vector
                Console.WriteLine("Second vector:");
                foreach (var element in vector2)
                {
                    Console.WriteLine("{0} ", element.ToString());
                }

                try
                {
                    // Checking two vectors whether they are ortogonal
                    if(Vector.AreOrtogonal(vector1,vector2))
                    {
                        Console.WriteLine("vector1 and vector2 are ortogonal.");
                    }
                    else
                    {
                        Console.WriteLine("vector1 and vector2 are not ortogonal.");

                        //Getting intersection of two non ortogonal vectors
                        Console.WriteLine("Intersection of two vectors is: {0}.", vector1.Intersection(vector2).ToString());

                    }

                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();

                    return;
                }

                //Output number of proper element in each vector
                Console.WriteLine("Vector1 has {0} elements '2'.",vector1.GetNumberOfElementsWithProperValue(2));
                Console.WriteLine("Vector2 has {0} elements '1'.", vector2.GetNumberOfElementsWithProperValue(1));

                //Creating third vector
                Vector vector3 = new Vector(1, 2, 3);

                //Output third vector
                Console.WriteLine("Third vector: ",vector3.ToString());
            }
            catch(ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                return;
            }

            Console.ReadLine();
        }
    }
}
