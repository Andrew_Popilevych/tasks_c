using System;
using System.Text;

namespace Task4
{
    /// <summary>
    /// Direction enum
    /// </summary>
    public enum Direction
    {
        Left = 1,
        Right = 2,
        Top = 3,
        Down = 4
    };

    /// <summary>
    /// Struct that represents point on area
    /// </summary>
    public struct Point
    {
       public double X { get; set; }
       public double Y { get; set; }

       public Point(double x, double y )
       {
           X = x;
           Y = y;
       }

        public override string ToString()
        {
            return string.Format("({0},{1})", X, Y);
        }
    }


    public class Rectangle
    {
        #region Fields

           private Point _point1;
           private Point _point2;
           private Point _point3;
           private Point _point4;

        #endregion

        #region Constructor
            
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="downLeft"> Down left point of rectangle</param>
            /// <param name="width"> Rectangle wigth</param>
            /// <param name="height">Rectangle height</param>
            public Rectangle(Point downLeft, double width, double height)
            {
                CreateNewRectangle(downLeft, width, height);
                Width = width;
                Height = height;
            }
            
            /// <summary>
            /// Overloaded constructor
            /// </summary>
            /// <param name="downLeft"> Down left point of rectangle</param>
            /// <param name="topRight">Top right point</param>
            public Rectangle(Point downLeft, Point topRight)
            {
                _point1 = downLeft;
                _point3 = topRight;
                _point2 = new Point(_point3.X, _point1.Y);
                _point4 = new Point(_point1.X, _point3.Y);

                Width = _point3.X - _point1.X;
                Height = _point3.Y - _point1.Y;
            }

        #endregion

        #region Properties

            /// <summary>
            /// Property for getting all rectangle points
            /// </summary>
            public Point[] RectanglePoints
            {
                get
                {
                    Point[] points = new Point[4];

                    points[0] = _point1;
                    points[1] = _point2;
                    points[2] = _point3;
                    points[3] = _point4;

                    return points;
                }
            }

            /// <summary>
            /// Get/set rectangle width
            /// </summary>
            public double Width { get; private set; }

            /// <summary>
            /// Get/set rectangle height
            /// </summary>
            public double Height { get; private set; }

        #endregion

        #region Methods
            
            /// <summary>
            /// Method for moving rectangle on proper value in certain direction
            /// </summary>
            /// <param name="value">Value for moving rectangle</param>
            /// <param name="direction">Direction enum</param>
            public void Move (double value, Direction direction)
            {
                switch(direction)
                {
                    case Direction.Left:
                        _point1 = new Point(_point1.X - value, _point1.Y);
                        _point3 = new Point(_point3.X - value, _point3.Y);

                        break;

                    case Direction.Right:
                        _point1 = new Point(_point1.X + value, _point1.Y);
                        _point3 = new Point(_point3.X + value, _point3.Y);

                        break;

                    case Direction.Top:
                        _point1 = new Point(_point1.X, _point1.Y + value);
                        _point3 = new Point(_point3.X, _point3.Y + value);

                        break;

                    case Direction.Down:
                        _point1 = new Point(_point1.X, _point1.Y - value);
                        _point3 = new Point(_point3.X, _point3.Y - value);

                         break;

                }

                _point2 = new Point(_point3.X, _point1.Y);
                _point4 = new Point(_point1.X, _point3.Y);

        }
            
            /// <summary>
            /// Method for changing rectangle size
            /// </summary>
            /// <param name="newWidth"> New width</param>
            /// <param name="newHeight"> New height</param>
            public void ChangeRectangleSizeTo (double newWidth, double newHeight)
            {
                 CreateNewRectangle(_point1, newWidth, newHeight);
            }

            /// <summary>
            /// Method for creating new rectangle which holds two other rectangles
            /// </summary>
            /// <param name="rectangle1"> First rectangle</param>
            /// <param name="rectangle2"> Second rectangle </param>
            /// <returns>Created rectangle instance</returns>
            public static Rectangle CreateNewRectangleWitchHoldsTwoAnother(Rectangle rectangle1, Rectangle rectangle2)
            {
                double downLeftX = Math.Min(rectangle1.RectanglePoints[0].X, rectangle2.RectanglePoints[0].X);
                double downLeftY = Math.Min(rectangle1.RectanglePoints[0].Y, rectangle2.RectanglePoints[0].Y);

                double topRightX = Math.Max(rectangle1.RectanglePoints[2].X, rectangle2.RectanglePoints[2].X);
                double topRightY = Math.Max(rectangle1.RectanglePoints[2].Y, rectangle2.RectanglePoints[2].Y);

                return new Rectangle(new Point(downLeftX,downLeftY), new Point(topRightX,topRightY));
            }

            /// <summary>
            /// Method for creating new rectangle formed by the intersection of two other rectangles
            /// </summary>
            /// <param name="rectangle1">First rectangle</param>
            /// <param name="rectangle2">Second rectangle</param>
            /// <returns></returns>
            public static Rectangle Intersection (Rectangle rectangle1, Rectangle rectangle2)
            {
                double downLeftX = Math.Max(rectangle1.RectanglePoints[0].X, rectangle2.RectanglePoints[0].X);
                double downLeftY = Math.Max(rectangle1.RectanglePoints[0].Y, rectangle2.RectanglePoints[0].Y);

                double topRightX = Math.Min(rectangle1.RectanglePoints[2].X, rectangle2.RectanglePoints[2].X);
                double topRightY = Math.Min(rectangle1.RectanglePoints[2].Y, rectangle2.RectanglePoints[2].Y);

                if(topRightX >= downLeftX && topRightY >= downLeftY)
                {
                    return new Rectangle(new Point(downLeftX, downLeftY), new Point(topRightX, topRightY));
                }
                else
                {
                    throw new InvalidOperationException("Rectangels does not intersect.");
                }
            }

            /// <summary>
            /// Method for representation rectangle as string
            /// </summary>
            /// <returns>Rectangle's points</returns>
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("Down Left point: "+_point1.ToString() + " \n ");
                sb.Append("Down Right point: " + _point2.ToString() + " \n ");
                sb.Append("Top Right point: " + _point3.ToString() + " \n ");
                sb.Append("Top Left point: " + _point4.ToString() + "\n ");

                return sb.ToString();
            }

        #endregion

        #region Helpers

            /// <summary>
            /// Method for ceating new rectangle
            /// </summary>
            /// <param name="downLeft"> Down left point</param>
            /// <param name="width"> Rectangle width</param>
            /// <param name="height">Rectangle height</param>
            private void CreateNewRectangle(Point downLeft, double width, double height)
            {
                 _point1 = downLeft;
                 _point2 = new Point(downLeft.X + width, downLeft.Y);
                 _point3 = new Point(_point2.X, _point2.Y+height);
                 _point4 = new Point(_point1.X, _point3.Y);
            }
     
        #endregion

    }
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rectangle1 = new Rectangle(new Point(1, 1), new Point(4, 2));
            Rectangle rectangle2 = new Rectangle(new Point(-1, 0), 3, 4);

            Console.WriteLine(" Your first rectangle: \n" + rectangle1.ToString());
            Console.WriteLine(" Your second rectangle: \n" + rectangle2.ToString());

            rectangle1.Move(1, Direction.Top);
            Console.WriteLine("First rectangle is moved on one point above. New rectangle: \n" + rectangle1.ToString());

            rectangle2.ChangeRectangleSizeTo(5, 3);
            Console.WriteLine("Second rectangle has new size: widht = 5, height = 3.");
            Console.WriteLine("Second rectangle: \n" + rectangle2.ToString());

            Rectangle smallRectangle = Rectangle.CreateNewRectangleWitchHoldsTwoAnother(rectangle1, rectangle2);
            Console.WriteLine("Rectangle that holds first and second rectangles:\n" + smallRectangle.ToString());

            try
            {
                Rectangle intersectionRectangle = Rectangle.Intersection(rectangle1, rectangle2);
                Console.WriteLine("Intersection rectangle is:\n" + intersectionRectangle.ToString());
            }catch(InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                return;
            }
            Console.ReadLine();
        }
    }
}
